import React from 'react';
import AppNavigator from './AppNavigator';
import { Provider } from 'react-redux';

export default class App extends React.Component {
  render() {
    return (
      <AppNavigator />
    );
  }
}