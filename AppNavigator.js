import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomePage from './src/component/HomePage/HomePage';
import DrawerLeft from './src/component/DrawerLeft/DrawerLeft';
import FlatList from './src/component/FlatList/FlatList';
import Modal from './src/component/Modal/Modal';
import Picker from './src/component/Picker/Picker';
import RefreshList from './src/component/RefreshList/RefreshList';
import ListOfPlaces from './src/component/ListOfPlaces/ListOfPlaces';
const stack = createStackNavigator(
    {
        Home: { screen: HomePage },
        DrawerLeft: { screen: DrawerLeft },
        FlatList: { screen: FlatList },
        Modal: { screen: Modal },
        Picker: { screen: Picker },
        RefreshList: { screen: RefreshList },
        ListOfPlaces: { screen: ListOfPlaces }
    },
    {
        navigationOptions: {
            header: null
        }
    }
);
const AppNavigator = createAppContainer(stack);
export default AppNavigator;