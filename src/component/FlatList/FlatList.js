import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Button,
    Image,
    FlatList,
    ActivityIndicator,
    ImageBackground
} from 'react-native';


export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            dataSource: []
        }
    }
    componentDidMount() {
        return fetch('https://jsonplaceholder.typicode.com/users')
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false,
                    dataSource: responseJson,
                }, function () {
                    // console.log(this.state.dataSource);
                });

            })
            .catch((error) => {
                console.error(error);
            });
    }
    renderItem = (item) => {
        // console.log(item.item.name);
        <View>
            {/* <Image style={{ width: 100, height: 100 }} source={{ uri: item.thumbnailUrl }} /> */}
            <View>
                <Text>
                    {item.key}
                </Text>
            </View>
        </View>
    }
    render() {
        console.log(this.state.dataSource);
        if (this.state.isLoading) {
            return (
                <ActivityIndicator size="large" color="#0000ff" />
            )
        } else {
            return (
                <ImageBackground source={{ uri: 'https://via.placeholder.com/600/92c952' }} style={{ width: '100%', height: '100%' }}>

                    <FlatList
                        data={[{ key: 'a' }, { key: 'b' }]}
                        // renderItem={({ item }) => <Text>{item.key}</Text>}
                        // data={this.state.dataSource}
                        renderItem={this.renderItem}
                    // keyExtractor={item => item.id.toString()}
                    />
                    <Image style={{ width: 100, height: 100 }} source={{ uri: 'https://via.placeholder.com/600/92c952' }} />
                </ImageBackground>
            );
        }
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF'
    }
})
