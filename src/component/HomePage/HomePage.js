import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { store } from '../../../store';
export default class App extends React.Component {

    constructor(props) {
        super(props);
    }

    render(props) {
        const { navigate } = this.props.navigation;
        return (
            <View>
                <Button
                    onPress={() => navigate('DrawerLeft')}
                    title="Drawer"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />
                <Button
                    onPress={() => navigate('FlatList')}
                    title="FlatList"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />
                <Button
                    onPress={() => navigate('Modal')}
                    title="Modal"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />
                <Button
                    onPress={() => navigate('Picker')}
                    title="Picker"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />
                <Button
                    onPress={() => navigate('RefreshList')}
                    title="RefreshList"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />
                <Button
                    onPress={() => navigate('ListOfPlaces')}
                    title="ListOfPlaces"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />
            </View>
        );
    }
}