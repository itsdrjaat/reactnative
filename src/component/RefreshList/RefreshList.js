import React from 'react';
import {
    View, Text,
    StyleSheet,
    Button,
    ActivityIndicator,
    ScrollView,
    RefreshControl
} from 'react-native';

export default class App extends React.Component {
    constructor() {
        super();
        this.state = {
            users: [],
            loading: true,
            refreshing: false,
        };
    }

    componentDidMount() {
        var url = 'https://jsonplaceholder.typicode.com/users';
        // var url = 'http://www.mocky.io/v2/5c875487320000af133bd147';
        return fetch(url)
            .then((response) => response.json())
            .then((data) => {
                this.setState({
                    users: data,
                    loading: false
                }, function () {
                    console.log(this.state.users);
                });
            })
    }
    renderUserList = () => {
        console.log(this.state.users);
        return this.state.users.map(user =>
            <View style={styles.container} key={user.id}>
                <Text>{user.name}</Text>
                <Text>{user.email}</Text>
            </View>
        )
    }

    _onRefresh = () => {
        this.state.refreshing = true;
        console.log(this.state.refreshing);
        var url = 'http://www.mocky.io/v2/5c875728320000f8143bd15a';
        return fetch(url)
            .then((response) => response.json())
            .then((data) => {
                console.log(data);
                this.setState({
                    users: data,
                    loading: false,
                    refreshing: false
                }, function () {
                    console.log(this.state.users);
                    console.log(this.state.refreshing);
                });
            })
    }

    render() {
        if (this.state.loading) {
            return (
                <View style={[styles.spinner, styles.horizontal]}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            );
        } else {
            return (
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }>
                    {this.renderUserList()}
                </ScrollView >
            );
        }
    }
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
    },
    spinner: {
        flex: 1,
        justifyContent: 'center'
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    }
});
