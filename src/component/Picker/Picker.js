import React from 'react';
import { View, Picker } from 'react-native';

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            language: ''
        }
    }
    render() {
        return (
            <View>
                <Picker
                    selectedValue={this.state.language}
                    style={{ height: 50, width: 100 }}
                    onValueChange={(itemValue, itemIndex) =>
                        this.setState({ language: itemValue })
                    }>
                    <Picker.Item label="Java" value="java" />
                    <Picker.Item label="JavaScript" value="js" />
                    <Picker.Item label="C" value="c" />
                    <Picker.Item label="Cpp" value="cpp" />
                </Picker>
            </View>
        )
    }
}
